<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\StoreProduct;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\StoreProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Store Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="store-product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Store Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'product_id',
            [
                'label' => 'Image',
                'format' => 'html',
                'value' => function ($data) {
                    return Html::img(DIRECTORY_SEPARATOR . StoreProduct::IMAGE_DIR . $data->product_image,
                        ['width' => '100px']);
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
