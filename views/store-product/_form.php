<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\StoreProduct */
/* @var $form yii\widgets\ActiveForm */
/* @var $products array */
/* @var $isUpdate bool */
?>

<div class="store-product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_id')
        ->widget(Select2::class, [
            'data' => $products,
            'options' => ['placeholder' => 'Choose product ...'],
            'pluginOptions' => [
                'allowClear' => true,
            ]]); ?>

    <?php if (!$isUpdate): ?>
        <?= $form->field($model, 'imageFile')->fileInput() ?>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
