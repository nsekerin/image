<?php

use \yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <p><?= Html::a('Product', ['product/index']) ?></p>
            <p><?= Html::a('Store Product', ['store-product/index']) ?></p>
        </div>

    </div>
</div>
