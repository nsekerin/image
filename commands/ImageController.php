<?php

namespace app\commands;

use app\models\Product;
use app\utilities\Images;
use yii\console\Controller;
use yii\console\ExitCode;

class ImageController extends Controller
{
    public function actionResize($sizes, $watermarked = false, $catalogOnly = true)
    {
        $generatedSuccessfully = 0;
        $generatedWithErrors = 0;

        $sizes = explode(',', $sizes);
        foreach ($sizes as $sizeOptions) {
            $sizeOptions = explode('x', $sizeOptions);
            if (count($sizeOptions) === 1) {
                $sizeOptions[1] = $sizeOptions[0];
            }
            $sizeOptions = ['width' => $sizeOptions[0], 'height' => $sizeOptions[1]];

            $imagesSources = [];
            /** @var Product[] $images */
            $images = Product::getProductsForImageResize($catalogOnly == true);
            foreach ($images as $image) {
                $imagesSources[] = $image->getImageAddress();
                foreach ($image->storeProducts as $storeProduct) {
                    $imagesSources[] = $storeProduct->getImageAddress();
                }
            }

            foreach ($imagesSources as $imagesSource) {
                try {
                    $miniatureSource = ($watermarked == true)
                        ? Images::generateWatermarkedMiniature($imagesSource, $sizeOptions)
                        : Images::generateMiniature($imagesSource, $sizeOptions, false);
                    $generatedSuccessfully++;
                } catch (\Exception $e) {
                    $generatedWithErrors++;
                }
            }
        }

        echo "Miniatures generated successfully = {$generatedSuccessfully}, miniatures generated with errors = {$generatedWithErrors}\n";

        return ExitCode::OK;
    }
}
