<?php

namespace app\utilities;

use Intervention\Image\ImageManagerStatic as Image;
use yii\base\InvalidArgumentException;

class Images
{
    public static $index = 0;

    public static function generateMiniature($imageSource, $options, $insertWatermark)
    {
        Image::configure(array('driver' => 'imagick'));

        if (empty($options['width']) || empty($options['height'])) {
            throw new InvalidArgumentException();
        }

        $img = Image::make('web/' . $imageSource);

        $currentHeight = $img->getHeight();
        $currentWidth = $img->getWidth();

        if ($currentHeight > $currentWidth) {
            $img->resize(null, $options['height'], function ($constraint) {
                $constraint->aspectRatio();
            });
        } else {
            $img->resize($options['width'], null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        if ($insertWatermark === true) {
            $img->insert('web/uploads/watermark.png');
        }

        static::$index++;
        $miniatureSource = 'web/uploads/miniatures/' . static::$index . '-' . $img->basename;
        // finally we save the image as a new file
        $img->save($miniatureSource);

        return $miniatureSource;
    }

    /**
     * @param $imageSource
     * @param $options
     * @return string
     */
    public static function generateWatermarkedMiniature($imageSource, $options)
    {
        return static::generateMiniature($imageSource, $options, true);
    }
}
