<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $image
 * @property int $is_deleted
 *
 * @property StoreProduct[] $storeProducts
 */
class Product extends \yii\db\ActiveRecord
{
    const IMAGE_DIR = 'uploads/product/';

    const STATUS_IS_DELETED = 1;
    const STATUS_IS_NOT_DELETED = 0;

    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_deleted'], 'integer'],
            [['image'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'is_deleted' => 'Is Deleted',
            'imageFile' => 'Image',
        ];
    }

    /**
     * @return bool
     */
    public function upload()
    {
        if ($this->validate() && !empty($this->imageFile)) {
            $fileName = $this->imageFile->baseName . '.' . $this->imageFile->extension;
            $result = $this->imageFile->saveAs(self::IMAGE_DIR . $fileName);

            if ($result) {
                $this->image = $fileName;

                return $this->save(false);
            }
        }

        return false;
    }

    /**
     * Gets query for [[StoreProducts]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStoreProducts()
    {
        return $this->hasMany(StoreProduct::className(), ['product_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getImageAddress()
    {
        return self::IMAGE_DIR . $this->image;
    }

    /**
     * @return mixed
     */
    public static function getAllForDropdownList()
    {
        $result = [];
        $products = self::find()->all();

        /** @var Product $product */
        foreach ($products as $product) {
            $result[$product->id] = $product->image;
        }

        return $result;
    }

    /**
     * @param bool $includeStoreProducts
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getProductsForImageResize($includeStoreProducts = true)
    {
        $images = self::find()
            ->alias('p')
            ->where('p.is_deleted = :isDeleted')
            ->params(['isDeleted' => self::STATUS_IS_NOT_DELETED]);

        if ($includeStoreProducts === true) {
            $images->innerJoin(StoreProduct::tableName() . ' sp', 'sp.product_id = p.id');
        } else {
            $images->leftJoin(StoreProduct::tableName() . ' sp', 'sp.product_id = p.id')
                ->andWhere('sp.id is null');
        }

        return $images->with('storeProducts')->all();
    }
}
