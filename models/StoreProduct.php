<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "store_product".
 *
 * @property int $id
 * @property int $product_id
 * @property string $product_image
 *
 * @property Product $product
 */
class StoreProduct extends \yii\db\ActiveRecord
{
    const IMAGE_DIR = 'uploads/store_product/';

    /**
     * @var UploadedFile
     */
    public $imageFile;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'store_product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id'], 'required'],
            [['product_id'], 'integer'],
            [['product_image'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['imageFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'imageFile' => 'Product Image',
        ];
    }

    /**
     * @return bool
     */
    public function upload()
    {
        if ($this->validate() && !empty($this->imageFile)) {
            $fileName = $this->imageFile->baseName . '.' . $this->imageFile->extension;
            $result = $this->imageFile->saveAs(self::IMAGE_DIR . $fileName);

            if ($result) {
                $this->product_image = $fileName;

                return $this->save(false);
            }
        }

        return false;
    }

    /**
     * @return string
     */
    public function getImageAddress()
    {
        return self::IMAGE_DIR . $this->product_image;
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
