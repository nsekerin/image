<?php

use yii\db\Migration;

/**
 * Class m200218_151930_init_product_and_store_product_tables
 */
class m200218_151930_init_product_and_store_product_tables extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string(255)->notNull(),
            'is_deleted' => $this->boolean()->notNull()->defaultValue(0),
        ]);

        $this->createTable('{{%store_product}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer()->notNull(),
            'product_image' => $this->string(255)->notNull(),
        ]);

        $this->addForeignKey(
            'fk_store_product_product_id',
            'store_product',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_store_product_product_id', 'store_product');

        $this->dropTable('store_product');
        $this->dropTable('product');
    }
}
